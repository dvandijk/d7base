require "susy"

if environment == :development
  require "compass-notify"
end

http_path         = "../"
css_dir           = "css"
sass_dir          = "scss"
images_dir        = "img"
javascripts_dir   = "js"
fonts_dir         = "fonts"

output_style      = (environment == :development) ? :expanded : :compressed
project_type      = :stand_alone
line_comments     = (environment == :development) ? :true : :false
preferred_syntax  = :scss
